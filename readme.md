# jm-carousel

a simple content carousel.

## usage

    let carousel = new Carousel({
      container: '.carousel',
      tray: '.carousel__tray',
      slideItem: '.carousel__item',
      nav: {
        next: '.carousel__nav--next',
        previous: '.carousel__nav--previous'
      }
    })

## demo
[https://jamesmaclennan.bitbucket.io/jm-carousel/](https://jamesmaclennan.bitbucket.io/jm-carousel/)