import TweenLite from 'gsap';

class Carousel {
  constructor(config) {
    this.slides;
    this.parentContainer = document.querySelector(config.container);
    this.tray = document.querySelector(config.tray);
    this.slideItem = config.slideItem;
    this.navInput = {
      next: document.querySelector(config.nav.next),
      previous: document.querySelector(config.nav.previous)
    }
    
    this.init();
  }
  init() {
    this.slides = Array.from(this.tray.childNodes).filter(slide => {
      return slide.nodeType != 3
    });

    // initialize slides and width of tray;
    let trayWidth = 0;
    this.slides.forEach((slide, i) => {
      trayWidth += slide.offsetWidth + 20;
      slide.setAttribute('data-name', 'slide-' + ++i)
      slide.addEventListener('click', this.navSlideClick.bind(this))
    })
    this.tray.style.width = trayWidth + 'px';

    // initialize nav
    this.navInput.next.addEventListener('click', this.navNext.bind(this));
    this.navInput.previous.addEventListener('click', this.navPrevious.bind(this));
    
    // set active slide
    this.slides[0].classList.add('is-active');
    this.slideSeek(this.tray.querySelector('.is-active'), true)
  }

  navNext() {
    this.slideSeek(this.tray.querySelector('.is-active').nextElementSibling);
  }

  navPrevious() {
    this.slideSeek(this.tray.querySelector('.is-active').previousElementSibling);
  }

  navSlideClick(slide) {
    this.slideSeek(slide.target.closest(this.slideItem));
  }

  // controls navigation button
  navCheck() {
    let activeIndex = this.slides.findIndex(function(x) {
      return x.classList.contains('is-active');
    })
    this.navInput.previous.disabled = (activeIndex == 0);
    this.navInput.next.disabled = (activeIndex == this.slides.length -1);
  }

  // moves carousel tray to position active slide
  slideSeek(slide, skipAnimation) {
    let target = slide;
    
    this.tray.querySelector('.is-active').classList.remove('is-active', 'is-animation-complete')
    target.classList.add('is-active')

    this.navCheck();

    // get target element if slide arrives as a click event
    if (slide.target) {
      target = slide.target.closest(this.slideItem);
    }

    // returns the translateX value of supplied element
    let getTranslateX = function(element) {
      let x = window.getComputedStyle(element, null);
      return x.transform.match(/\d+/g)[4]
    }

    let x = ((this.parentContainer.clientWidth - target.clientWidth)/2) - target.offsetLeft;
    if (skipAnimation) {
      TweenLite.set(this.tray, {x:x})
      slide.classList.add('is-animation-complete');
    } else {
      TweenLite.to(this.tray, .25, {x: x, onComplete: function() {
        slide.classList.add('is-animation-complete');
      }})
    }
  }
}

export default Carousel